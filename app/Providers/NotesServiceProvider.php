<?php
declare(strict_types=1);
/*
 * Copyright Jake Round
 */

namespace App\Providers;

use App\Repositories\Contracts\NotesRepositoryContract;
use App\Repositories\NotesRepository;
use Illuminate\Support\ServiceProvider;

/**
 * Class NotesServiceProvider
 * @package App\Providers
 * @author Jake Round
 */
class NotesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            NotesRepositoryContract::class,
            NotesRepository::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
