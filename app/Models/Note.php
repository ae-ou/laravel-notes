<?php
declare(strict_types=1);
/*
 * Copyright Jake Round
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Note
 * @package App\Models
 * @author Jake Round
 */
class Note extends Model
{
    protected $fillable = [
        'title',
        'body',
        'user_id'
    ];

    /**
     * Get the user that the made the note.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
