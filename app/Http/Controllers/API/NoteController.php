<?php
declare(strict_types=1);
/*
 * Copyright Jake Round
 */

namespace App\Http\Controllers\API;

use App\Http\Requests\Notes\CreateNote;
use App\Http\Requests\Notes\DeleteNote;
use App\Models\Note;
use App\Repositories\Contracts\NotesRepositoryContract;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Exception;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class NoteController
 *
 * @package App\Http\API\Controllers
 * @author  Jake Round
 */
class NoteController extends Controller
{
    protected $notesRepository;

    /**
     * NoteController constructor.
     *
     * @param NotesRepositoryContract $notesRepository
     */
    public function __construct(NotesRepositoryContract $notesRepository)
    {
        $this->notesRepository = $notesRepository;
    }

    /**
     * Create a new note
     *
     * @param CreateNote $request
     *
     * @return JsonResponse
     */
    public function create(CreateNote $request): JsonResponse
    {
        $note = $this->notesRepository->create(auth()->user()->id, $request->input('title'), $request->input('body'));

        return response()->json([
                                    'id' => $note->id,
                                ]);
    }

    /**
     * @param Note $note
     *
     * @return JsonResponse
     */
    public function view(Note $note): JsonResponse
    {
        return response()->json($note);
    }

    /**
     * @param Note       $note
     * @param DeleteNote $request
     *
     * @return JsonResponse
     */
    public function delete(Note $note, DeleteNote $request): JsonResponse
    {
        //TODO: Switch the DeleteNote request to a gate, since there aren't any request params to validate.
        try {
            $this->notesRepository->delete($note->id);
        } catch (Exception $exception) {
            $responseCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return response()->json([], $responseCode ?? Response::HTTP_OK);
    }


}
