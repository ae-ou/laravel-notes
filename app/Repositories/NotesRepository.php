<?php
declare(strict_types=1);
/*
 * Copyright Jake Round
 */


namespace App\Repositories;


use App\Models\Note;
use App\Repositories\Contracts\NotesRepositoryContract;
use Carbon\Carbon;
use Exception;
use OutOfBoundsException;

/**
 * Class NotesRepository
 * @package App\Repositories
 * @author Jake Round
 **/
class NotesRepository implements NotesRepositoryContract
{
    /**
     * @var Note $noteModel
     */
    protected $noteModel;

    public function __construct(Note $noteModel)
    {
        $this->noteModel = $noteModel;
    }

    /**
     * {@inheritDoc}
     */
    public function create(int $userId, string $title, string $body): Note
    {
        return $this->noteModel->create([
            'user_id' => $userId,
            'title' => $title,
            'body' => $body,
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function delete(int $noteId): void
    {
        if ($noteId <= 0) {
            throw new OutOfBoundsException('Number of days must be a positive integer');
        }

        try {
            $this->noteModel
                ->find($noteId)
                ->delete();
        } catch (Exception $exception) {
            throw new Exception('Deletion could not be performed');
        }
    }

    /**
     * {@inheritDoc}
     */
    public function deleteOlderThan(int $numberOfDays = 30, bool $deleteUpToCurrentTime = false): void
    {
        if ($numberOfDays <= 0) {
            throw new OutOfBoundsException('Number of days must be a positive integer');
        }

        $currentDateTime = Carbon::now();
        $createdEarlierThan = $currentDateTime->subDays($numberOfDays);

        if (!$deleteUpToCurrentTime) {
            $createdEarlierThan->startOfDay();
        }

        try {
            $this->noteModel->where('created_at', '<', $createdEarlierThan->format('Y-m-d H:i:s'))->delete();
        } catch (Exception $exception) {
            throw new Exception('Deletion could not be performed');
        }
    }
}
