<?php
declare(strict_types=1);
/*
 * Copyright Jake Round
 */

namespace App\Repositories\Contracts;

use App\Models\Note;
use Exception;

/**
 * Interface NotesRepositoryContract
 * @package App\Repositories\Contracts
 */
interface NotesRepositoryContract
{
    /**
     * Create a new note.
     *
     * @param int $userId
     * @param string $title
     * @param string $body
     * @return Note
     */
    public function create(int $userId, string $title, string $body): Note;

    /**
     * Delete the specified note.
     *
     * @param int $noteId
     * @throws Exception
     */
    public function delete(int $noteId): void;

    /**
     * Delete notes that were created more than x days ago.
     *
     * @param int $numberOfDays
     * @param bool $deleteUpToCurrentTime - if set to true, we delete notes created up to the current time $numberOfDays
     * days ago. If set to false, we get the date $numberOfDays days ago, and only delete notes created before 00:00 on
     * that date (so, notes potentially 23:59 older than the number of days specified by $numberOfDays)
     * @throws Exception
     * @return void
     */
    public function deleteOlderThan(int $numberOfDays = 30, bool $deleteUpToCurrentTime = false): void;
}
