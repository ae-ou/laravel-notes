<?php
declare(strict_types=1);
/*
 * Copyright Jake Round
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateNotesTable
 * @author Jake Round
 */
class CreateNotesTable extends Migration
{
    protected $table = 'notes';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 100)
                  ->unique()
                  ->nullable(false);
            $table->string('body',2500)
                  ->nullable(false);
            $table->integer('user_id')
                  ->unsigned();
            $table->index('user_id');
            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
