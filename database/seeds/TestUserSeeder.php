<?php
declare(strict_types=1);

/*
 * Copyright Jake Round
 */

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Note;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserNotesSeeder
 * @author Jake Round
 */
class TestUserSeeder extends Seeder
{
    /**
     * DO NOT RUN ON LIVE
     * ******************
     *  Create a user where the auth credentials are known.
     *  This file isn't referenced in DatabaseSeeder, so it won't run by default. You have to call:
     *  php artisan db:seed --class=TestUserSeeder
     * ******************
     * DO NOT RUN ON LIVE
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'username' => 'DevelopmentTestUser',
            'email' => 'example@test.com',
            'password' => Hash::make('password'),
        ]);
    }
}
