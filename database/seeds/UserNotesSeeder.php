<?php
declare(strict_types=1);
/*
 * Copyright Jake Round
 */
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Note;

/**
 * Class UserNotesSeeder
 * @author Jake Round
 */
class UserNotesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 25)->create()->each(function ($user) {
            $user->notes()->save(factory(Note::class)->make());
        });
    }
}
