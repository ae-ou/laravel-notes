<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Models\Note;

$factory->define(Note::class, function (Faker $faker) {
    return [
        'title' => $faker->unique()->text(100),
        'body' => $faker->text(2500),
    ];
});
