<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api', 'namespace' => 'API', 'as' => 'api.'], function () {

    Route::group(['prefix' => 'auth', 'as' => 'auth.'], function () {
        Route::post('login', 'AuthController@login')->name('login');
        Route::post('logout', 'AuthController@logout')->name('logout');
        Route::post('refresh', 'AuthController@refresh')->name('refresh');
        Route::get('me', 'AuthController@me')->name('me');
    });

    Route::group(['prefix' => 'note', 'as' => 'note.'], function () {
        Route::get('view/{note}', 'NoteController@view')->name('view');

        Route::group(['middleware' => 'auth:api'], function () {
            Route::post('create', 'NoteController@create')->name('create');
            Route::delete('delete/{note}', 'NoteController@delete')->name('delete');
        });
    });
});
