# Notes (Unfinished)
Demo app to showcase some Laravel functionality.

## Using JWT:
This project is an API, so the plan was to use `laravel/passport` for authentication via OAuth
since the default API authentication provided by Laravel (https://laravel.com/docs/5.8/api-authentication) is 
woefully inadequate.

Since this is my first time using passport with an SPA, I looked into the best OAuth grant type to use.

The POST `oauth/token` route (for the password grant - i.e. the grant that returns tokens to a user when they 
authenticate with email/password) requires the client’s `secret_key` on the request. If the client is a JS SPA, 
sending the `secret_key` would mean either storing it in the front end (obviously bad), or routing the request through 
a broker that could add the `secret_key` parameter onto the request and forward it to the API - before returning the 
response to the spa (overcomplicated, and with security issues of its own).

Passport does support the Implicit Grant Type (https://laravel.com/docs/5.8/passport#implicit-grant-tokens) as per the 
OAuth2 spec (https://developer.okta.com/blog/2018/05/24/what-is-the-oauth2-implicit-grant-type) - the issue is that 
using it is discouraged by OAuth (https://oauth.net/2/grant-types/implicit/) because it has numerous vulnerabilities 
(https://tools.ietf.org/html/draft-ietf-oauth-security-topics-13#section-3.1.2).

The recommended authorisation flow for SPA apps is Authorisation Code Grant with PKCE (https://oauth.net/2/pkce/), 
but this functionality has only recently been merged into `thephpleague/oauth2server` - a package that 
`laravel/passport` is dependent on (https://github.com/laravel/passport/blob/7.0/composer.json). There is currently a 
pull request on the Laravel Passport repo to add PKCE support (https://github.com/laravel/passport/pull/1065), but the
pull request wasn't approved at the time that I was working on this project.

The decision was made to use `tymon/jwt-auth` because it allows SPA authentication using a username/password, and
it doesn't suffer from the same security issue as `laravel/passport`.