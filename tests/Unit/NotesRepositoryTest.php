<?php

namespace Tests\Unit;

use App\Models\Note;
use App\Models\User;
use App\Repositories\NotesRepository;
use Carbon\CarbonImmutable;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NotesRepositoryTest extends TestCase
{
    use RefreshDatabase;
    protected static $user;
    protected static $notesRepository;
    protected static $currentTime;

    public function setUp(): void
    {
        parent::setUp();

        self::$notesRepository = new NotesRepository(new Note());

        self::$currentTime = CarbonImmutable::now();

        self::$user = factory(User::class, 1)->create()->first();
    }

    /**
     * Check that the repository can accurately store the note and return a model.
     *
     * @return void
     */
    public function testCreate()
    {
        $title = 'Test Title';
        $body = 'Test body.';
        $createResponse = self::$notesRepository->create(self::$user->id, $title, $body);

        self::assertInstanceOf(Note::class, $createResponse);
        self::assertEquals(self::$user->id, $createResponse->user_id);
        self::assertEquals($title, $createResponse->title);
        self::assertEquals($body, $createResponse->body);
        self::assertDatabaseHas($createResponse->getTable(), $createResponse->toArray());
    }

    /**
     * Check that the repository can delete the note specified by the id.
     */
    public function testDelete()
    {
        //Create a note tied to the test user.
        $note = factory(Note::class, 1)->create([
            'user_id' => self::$user->id,
        ])->first();

        //Ensure that the note was actually created (Laravel methods are tested already).
        self::assertDatabaseHas($note->getTable(), $note->toArray());

        //Delete the note from the DB using the repository.
        self::$notesRepository->delete($note->id);

        //Check that the record that was in the DB is now gone following the repository delete command.
        self::assertDatabaseMissing($note->getTable(), $note->toArray());
    }

    /**
     * Create a list of notes with different created_at timestamps.
     *
     * @return array
     */
    protected function createNotesWithVaryingTimestamps()
    {
        $createdAtValues = [
            'now' => self::$currentTime->format('Y-m-d H:i:s'),
            'current_time_thirty_days_ago' => self::$currentTime->subDays(30)->format('Y-m-d H:i:s'),
            'midnight_thirty_days_ago' => self::$currentTime->subDays(30)->startOfDay()->format('Y-m-d H:i:s'),
            'before_thirty_days_ago' => self::$currentTime->subDays(31)->format('Y-m-d H:i:s'),
        ];

        $notes = [];

        foreach ($createdAtValues as $key => $value) {
            $notes[$key] = factory(Note::class, 1)->create([
                'user_id' => self::$user->id,
                'created_at' => $value
            ])->first();
        }

        return $notes;
    }

    /**
     * Check that notes made before midnight 30 days ago are deleted.
     */
    public function testDeleteOlderThanMidnightThirtyDaysAgo()
    {
        $notes = $this->createNotesWithVaryingTimestamps();

        //Check that all of the notes that were generated are still in the table.
        foreach ($notes as $note) {
            self::assertDatabaseHas($note->getTable(), $note->toArray());
        }

        //Delete the note created before midnight 30 days ago ($notes['before_thirty_days_ago'])
        self::$notesRepository->deleteOlderThan(30, false);

        //Check that all the note made before midnight 30 days ago was deleted.
        foreach ($notes as $key => $note) {
            if ($key === 'before_thirty_days_ago') {
                self::assertDatabaseMissing($note->getTable(), $note->toArray());
            } else {
                self::assertDatabaseHas($note->getTable(), $note->toArray());
            }
        }
    }

    /**
     * Check that notes older than the current time 30 days ago are deleted.
     */
    public function testDeleteOlderThanCurrentTimeThirtyDaysAgo()
    {
        $notes = $this->createNotesWithVaryingTimestamps();

        //Check that all of the notes that were generated are still in the table.
        foreach ($notes as $note) {
            self::assertDatabaseHas($note->getTable(), $note->toArray());
        }

        //Delete the notes created the current time 30 days ago ($notes['before_thirty_days_ago'], and $notes['midnight_thirty_days_ago'])
        self::$notesRepository->deleteOlderThan(30, true);

        //Check that the notes generated before self::$currentTime - 30 days were deleted.
        foreach ($notes as $key => $note) {
            if (in_array($key, ['before_thirty_days_ago', 'midnight_thirty_days_ago'])  ) {
                self::assertDatabaseMissing($note->getTable(), $note->toArray());
            } else {
                self::assertDatabaseHas($note->getTable(), $note->toArray());
            }
        }
    }

    public function tearDown(): void
    {
        parent::tearDown(); // TODO: Change the autogenerated stub
    }
}
