<?php

namespace Tests\Feature;

use App\Models\Note;
use App\Models\User;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Hash;
use Tests\AbstractTests\AuthedTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiNoteControllerTest extends AuthedTestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * An first user
     *
     * @var User
     */
    protected $firstUser;

    /**
     * A note for the user defined in AuthedTestCase
     *
     * @var Note
     */
    protected $firstUserNote;

    /**
     * An additional user
     *
     * @var User
     */
    protected $secondUser;

    /**
     * A note for the additional user
     *
     * @var Note
     */
    protected $secondUserNote;


    protected function setUp(): void
    {
        parent::setUp();
        $this->firstUser = factory(User::class, 1)->create([
                                                          'username' => self::$username,
                                                          'email'    => self::$email,
                                                          'password' => Hash::make(self::$password),
        ])->each(function ($user) {
            $user->notes()->save(factory(Note::class)->make());
        })->first();

        $this->firstUserNote = $this->firstUser->notes()->first();

        $this->secondUser = factory(User::class, 1)->create()->each(function ($user) {
            $user->notes()->save(factory(Note::class)->make());
        })->first();

        $this->secondUserNote = $this->secondUser->notes()->first();

    }

    /**
     * Make sure that an authenticated user can create a note.
     */
    public function testCreateNote()
    {
        $loginResponse = $this->logIn();

        $createNoteResponse = $this->json('POST', 'api/note/create', [
            'title' => 'This is the title',
            'body'  => 'This is the body',
        ]);

        $createNoteResponse->assertStatus(Response::HTTP_OK);
    }

    /**
     * If one the fields title/body is null, we should get an error in response.
     */
    public function testErrorForNullFieldsOnCreateNote()
    {
        $loginResponse = $this->logIn();

        $createNoteResponse = $this->json('POST', 'api/note/create');

        $createNoteResponse->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testErrorForExcessiveFieldLengthOnCreateNote()
    {
        $loginResponse = $this->logIn();

        //Have to use sentence function because Faker doesn't allow you to specify minimum length on text().
        $createNoteResponse = $this->json('POST', 'api/note/create', [
            'title' => $this->faker->sentence(100),
            'body'  => $this->faker->sentence(2500),
        ]);

        $createNoteResponse->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Make sure that a logged out user can't create a note.
     */
    public function testUnauthenticatedCreateNote()
    {
        $createNoteResponse = $this->json('POST', 'api/note/create', [
            'title' => 'This is the title',
            'body'  => 'This is the body',
        ]);

        $createNoteResponse->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Make sure that an logged in user can delete their own note.
     */
    public function testDeleteNote()
    {
        $loginResponse = $this->logIn();

        $deleteNoteResponse = $this->json('DELETE', route('api.note.delete', $this->firstUserNote->id));

        $deleteNoteResponse->assertStatus(Response::HTTP_OK);
    }

    /**
     * Make sure that a logged out user can't delete a note.
     */
    public function testLoggedOutDeleteNote()
    {
        $deleteNoteResponse = $this->json('DELETE', route('api.note.delete', $this->firstUserNote->id));

        $deleteNoteResponse->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Make sure that an logged in user can't delete another user's note.
     */
    public function testDeleteDifferentUsersNote()
    {
        // Log in as user one.
        $loginResponse = $this->logIn();

        // Attempt to delete User two's note.
        $deleteNoteResponse = $this->json('DELETE', route('api.note.delete', $this->secondUserNote->id));

        $deleteNoteResponse->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /**
     * Check that a user can view a note.
     */
    public function testAuthenticatedViewNote()
    {
        $loginResponse = $this->logIn();

        $viewNoteResponse = $this->json('get', route('api.note.view', $this->secondUserNote->id));

        $viewNoteResponse->assertStatus(Response::HTTP_OK);

        $viewNoteResponse->assertJsonStructure(['id',
                                                'title',
                                                'body',
                                                'user_id',
                                                'created_at'
                                               ]);
    }

    /**
     * Check that an unauthenticated user can view a note.
     */
    public function testUnauthenticatedViewNote()
    {
        $viewNoteResponse = $this->json('get', route('api.note.view', $this->secondUserNote->id));

        $viewNoteResponse->assertStatus(Response::HTTP_OK);

        $viewNoteResponse->assertJsonStructure(['id',
                                                'title',
                                                'body',
                                                'user_id',
                                                'created_at'
                                               ]);
    }

    protected function tearDown(): void
    {
        parent::tearDown(); // TODO: Change the autogenerated stub
    }
}
