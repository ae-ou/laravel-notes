<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\AbstractTests\AuthedTestCase;

class ApiAuthControllerTest extends AuthedTestCase
{
    use RefreshDatabase;

    protected $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class, 1)->create([
                                                          'username' => self::$username,
                                                          'email'    => self::$email,
                                                          'password' => Hash::make(self::$password),
                                                      ])->first();
    }

    /**
     * Test that logging in actually works.
     */
    public function testLoginWithValidData()
    {
        $response = $this->logIn();

        $response->assertStatus(200);
        $this->assertEquals('bearer', $response->json('token_type'));
    }

    /**
     * Test that attempting to login with bad data fails.
     * This test class runs RefreshDatabase after each test, so this user will not exist.
     */
    public function testLoginWithInvalidData()
    {
        $response = $this->post('api/auth/login', [
            'email'    => 'InvalidUser',
            'password' => 'InvalidPassword',
        ]);

        $response->assertStatus(401);
        $this->assertEquals('Unauthorized', $response->json('error'));
    }

    /**
     * Check that the JWT token changes if an already authed user logs in again.
     */
    public function testLoginWhenLoggedIn()
    {
        $loginResponse = $this->logIn();
        $secondLoginResponse = $this->logIn();

        self::assertNotEquals($loginResponse->json('access_token'), $secondLoginResponse->json('access_token'));
    }

    public function testGetMe()
    {
        $loginResponse = $this->logIn();

        $meResponse = $this->get('api/auth/me', [
            'Authorization' => 'bearer ' . $loginResponse->json('access_token'),
        ]);

        $meResponse->assertStatus(200)->assertJson([
                                                       'username' => self::$username,
                                                       'email'    => self::$email,
                                                   ])->assertJsonMissing(['password']);

        //Doing separately because we don't have user id locally to compare against.
        self::assertArrayHasKey('id', $meResponse->json());
    }

    public function testRefreshToken()
    {
        $response = $this->logIn();

        $refreshResponse = $this->post('api/auth/refresh');

        $refreshResponse->assertStatus(200);
        self::assertNotEquals($response->json('access_token'), $refreshResponse->json('access_token'));
    }

    public function testLogOut()
    {
        $loginResponse = $this->logIn();

        $logoutResponse = $this->post('api/auth/logout', [
            'Authorization' => 'bearer ' . $loginResponse->json('access_token'),
        ]);

        $logoutResponse->assertStatus(200)->assertJson(['message' => 'Success']);
    }
}
