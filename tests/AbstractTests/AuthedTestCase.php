<?php

namespace Tests\AbstractTests;

use Illuminate\Foundation\Testing\TestResponse;
use Tests\TestCase;

abstract class AuthedTestCase extends TestCase {

    protected static $username = 'TestUser';
    protected static $email    = 'test@test.com';
    protected static $password = 'password';

    /**
     * Log in to the API
     *
     * @return TestResponse
     */
    protected function logIn(): TestResponse
    {
        $response = $this->post('api/auth/login', [
            'email'    => self::$email,
            'password' => self::$password,
        ]);

        return $response;
    }


}
